package inoco.quentin.incremental.kata;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import inoco.quentin.incremental.kata.exception.NegativeNumberException;
import inoco.quentin.incremental.kata.internationalization.Messages;

public class IncrementalCalculator {

	// pattern : \/\/(.*)\n([\s\S]*)
	private static final Pattern DELIMITER_PATTERN = Pattern.compile("\\/\\/(.*)\\n([\\s\\S]*)"); //$NON-NLS-1$

	/**
	 * 
	 * @param numbers String that contains numbers to add. by default, the delimiter
	 *                between numbers is ',', but the delimiter can be given at the
	 *                beginning with the format �//[delimiter]\n[numbers�]�
	 * @return the sum of numbers contained in parameter numbers, and 0 if numbers
	 *         is null or blank
	 * 
	 * @throws NegativeNumberException
	 */
	public int add(String numbers) throws NegativeNumberException {
		if (numbers == null || numbers.isBlank()) {
			return 0;
		}
		List<Integer> extractedNumbers = parseNumbers(numbers);
		checkNumbers(extractedNumbers);
		return extractedNumbers.stream().collect(Collectors.summingInt(Integer::intValue));
	}

	private List<Integer> parseNumbers(String input) {
		// analyze input to check if a delimiter is defined
		Matcher matcher = DELIMITER_PATTERN.matcher(input);
		String delimiter;
		String numbers;
		if (matcher.find()) {
			// delimiter is defined
			delimiter = matcher.group(1);
			numbers = matcher.group(2);
		} else {
			// default delimiter is ',' and input contains only numbers
			delimiter = ","; //$NON-NLS-1$
			numbers = input;
		}
		String[] splittedNumbers = numbers.split(String.format("[%n%s]", delimiter)); //$NON-NLS-1$
		return Arrays.stream(splittedNumbers).map(Integer::parseInt).toList();
	}

	private void checkNumbers(List<Integer> numbers) throws NegativeNumberException {
		List<Integer> negativeNumbers = numbers.stream().filter(number -> number < 0).toList();
		if (negativeNumbers.isEmpty()) {
			return;
		}
		String errorMessage = String.format(Messages.getString("IncrementalCalculator.negativeNumberErrorMessage"), //$NON-NLS-1$
				negativeNumbers.stream().map(Object::toString).collect(
						Collectors.joining(Messages.getString("IncrementalCalculator.negativeNumberDelimiter")))); //$NON-NLS-1$
		throw new NegativeNumberException(errorMessage);
	}

}
