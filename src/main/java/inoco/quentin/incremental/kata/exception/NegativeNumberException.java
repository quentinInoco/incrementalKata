package inoco.quentin.incremental.kata.exception;

public class NegativeNumberException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8215206131582535764L;

	public NegativeNumberException(String message) {
		super(message);
	}

}
