package inoco.quentin.incremental.kata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import inoco.quentin.incremental.kata.exception.NegativeNumberException;

class IncrementalCalculatorTest {

	private IncrementalCalculator objectToTest = new IncrementalCalculator();

	@Test
	@DisplayName("null, empty and string values")
	void testEmptyString() throws NegativeNumberException {
		assertEquals(0, objectToTest.add(null), "null value failed");
		assertEquals(0, objectToTest.add(""), "empty value failed");
		assertEquals(0, objectToTest.add("     "), "blank value failed");
	}

	@Test
	@DisplayName("accept 1 or 2 numbers")
	void testStep1() throws NegativeNumberException {
		assertEquals(45, objectToTest.add("45"), "1 number failed");
		assertEquals(515, objectToTest.add("56,459"), "2 numbers failed");
	}

	@Test
	@DisplayName("accept unknown amount of numbers")
	void testStep2() throws NegativeNumberException {
		Random randomGenerator = new Random();
		List<Integer> numbers = randomGenerator.ints(3 + randomGenerator.nextInt(500), 0, 100_000).boxed()
				.collect(Collectors.toList());
		int expectedSum = numbers.parallelStream().mapToInt(Integer::intValue).sum();
		String parameter = numbers.parallelStream().map(Object::toString).collect(Collectors.joining(","));
		assertEquals(expectedSum, objectToTest.add(parameter));
	}

	@Test
	@DisplayName("handle new lines between numbers")
	void testStep3() throws NegativeNumberException {
		assertEquals(6, objectToTest.add("1\n2,3"));
	}

	@Test
	@DisplayName("support different delimiters")
	void testStep4() throws NegativeNumberException {
		assertEquals(45, objectToTest.add("//;\n2;3\n40"));
	}

	@Test
	@DisplayName("Throw exception with negative numbers")
	void testStep5() {
		try {
			objectToTest.add("//;\n2;-3\n40;7;-89;916");
			fail("Should have throw NegativeNumberException");
		} catch (NegativeNumberException e) {
			assertEquals("negatives not allowed: -3; -89", e.getMessage());
		}
	}

}
