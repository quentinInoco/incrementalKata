# incrementalKata

This project is an implementation of the [string calculator data](https://kata-log.rocks/string-calculator-kata) inspired by Roy Osherove.

This project covers step 1 to step 5 included.

## Environment

This project use java 17, maven, junit 5, and jacoco to check the coverage.

## Running test and coverage

To execute the tests, you can :
* launch test class IncrementalCalculatorTest via your IDE
* execute the command mvn clean verify in a terminal (it will also launch jacoco and check that the coverage is greater than 80%)
